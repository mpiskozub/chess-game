CREATE TABLE IF NOT EXISTS pieces (
                                     id SERIAL PRIMARY KEY,
                                     x_position INTEGER NOT NULL,
                                     y_position INTEGER NOT NULL,
                                     piece_type VARCHAR(255),
                                     removed BOOLEAN
    );