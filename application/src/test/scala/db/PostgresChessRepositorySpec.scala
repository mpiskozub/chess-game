package db

import cats.effect.Resource
import containers.PostgresContainer
import domain._
import doobie.hikari.HikariTransactor
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.testcontainers.containers.PostgreSQLContainer
import zio.{Runtime, Unsafe}

class PostgresChessRepositorySpec extends AnyWordSpec with Matchers with BeforeAndAfterAll {

  val container: PostgreSQLContainer[?] = PostgresContainer.createPostgresContainer
  private lazy val transactor: Resource[zio.Task, HikariTransactor[zio.Task]] = PostgresContainer.createHikariTransactor(container)
  val runtime = Runtime.default

  override def beforeAll(): Unit = {
    super.beforeAll()
    container.start()
  }

  "A PostgresChessRepository" should {

    "add a piece" in {
      val chessRepository = PostgresChessRepositoryLive(transactor)
      val piece = Rook(PieceId(1), Position(0, 0))

      val result = chessRepository.addPiece(piece)

      val actualResult = Unsafe.unsafe { implicit unsafe =>
        runtime.unsafe.run(result).getOrThrowFiberFailure()
      }
      actualResult shouldBe Right(1)
    }

    "remove a piece" in {
      val chessRepository = PostgresChessRepositoryLive(transactor)
      val piece = Rook(PieceId(2), Position(1, 1))

      val addResult = chessRepository.addPiece(piece)
      val actualAddResult = Unsafe.unsafe { implicit unsafe =>
        runtime.unsafe.run(addResult).getOrThrowFiberFailure()
      }
      actualAddResult shouldBe Right(1)

      val removeResult = chessRepository.removePiece(piece.id)
      val actualRemoveResult = Unsafe.unsafe { implicit unsafe =>
        runtime.unsafe.run(removeResult).getOrThrowFiberFailure()
      }
      actualRemoveResult shouldBe Right(1)
    }

    "move a piece" in {
      val chessRepository = PostgresChessRepositoryLive(transactor)
      val piece = Rook(PieceId(3), Position(2, 2))

      val addResult = chessRepository.addPiece(piece)
      val actualAddResult = Unsafe.unsafe { implicit unsafe =>
        runtime.unsafe.run(addResult).getOrThrowFiberFailure()
      }
      actualAddResult shouldBe Right(1)

      val newPosition = Position(3, 3)
      val moveResult = chessRepository.movePiece(piece.id, newPosition)
      val actualMoveResult = Unsafe.unsafe { implicit unsafe =>
        runtime.unsafe.run(moveResult).getOrThrowFiberFailure()
      }
      actualMoveResult shouldBe Right(1)
    }

    "load the board" in {
      val chessRepository = PostgresChessRepositoryLive(transactor)
      val rook = Rook(PieceId(4), Position(4, 4))
      val bishop = Bishop(PieceId(5), Position(5, 5))

      Unsafe.unsafe { implicit unsafe =>
        runtime.unsafe.run(chessRepository.clearBoard).getOrThrowFiberFailure()
        runtime.unsafe.run(chessRepository.addPiece(rook)).getOrThrowFiberFailure()
        runtime.unsafe.run(chessRepository.addPiece(bishop)).getOrThrowFiberFailure()
      }

      val loadResult = chessRepository.loadBoard
      val actualLoadResult = Unsafe.unsafe { implicit unsafe =>
        runtime.unsafe.run(loadResult).getOrThrowFiberFailure()
      }

      actualLoadResult shouldBe Right(ChessBoard(Map(
        rook.id -> rook,
        bishop.id -> bishop
      )))
    }
  }



}
