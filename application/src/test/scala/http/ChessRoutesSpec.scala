package http

import domain.{AddPieceRequest, MovePieceRequest, PieceType, RemovePieceRequest}
import io.circe.syntax.EncoderOps
import mocks.{MockChessService, MockMessageProducer}
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.should.Matchers
import sttp.client3._
import sttp.client3.testing.SttpBackendStub
import sttp.model.MediaType
import sttp.tapir.server.stub.TapirStubInterpreter
import zio.{Runtime, ZIO, _}
import Implicits._
class ChessRoutesSpec extends AsyncFlatSpec with Matchers {
  val runtime: Runtime[Any] = Runtime.default
  val chessRoutes: ChessRoutes = ChessRoutes(ZIO.succeed(MockChessService), MockMessageProducer)

  "addPieceEndpoint" should "return success response for valid piece type" in {

    implicit val addPieceRequestSerializer: BodySerializer[AddPieceRequest] = { request: AddPieceRequest =>
      val serialized = request.asJson.noSpaces
      StringBody(serialized, "UTF-8", MediaType.ApplicationJson)
    }

    val backendStub = TapirStubInterpreter(SttpBackendStub.asynchronousFuture)
      .whenServerEndpoint(chessRoutes.addPieceEndpoint.serverLogic(request => Unsafe.unsafe(implicit unsafe => runtime.unsafe.runToFuture(chessRoutes.addPieceAndPropagateMessage(request)))))
      .thenRunLogic()
      .backend()

    val request: AddPieceRequest = AddPieceRequest(1, 0, 0, PieceType.Rook)

    val response = basicRequest
      .post(uri"http://test.com/app/addPiece")
      .body(request)(addPieceRequestSerializer)
      .send(backendStub)

    response.map(_.code.code shouldBe 200)
  }

  "movePieceEndpoint" should "return success response for valid move" in {
    implicit val movePieceRequestSerializer: BodySerializer[MovePieceRequest] = { request: MovePieceRequest =>
      val serialized = request.asJson.noSpaces
      StringBody(serialized, "UTF-8", MediaType.ApplicationJson)
    }

    val backendStub = TapirStubInterpreter(SttpBackendStub.asynchronousFuture)
      .whenServerEndpoint(chessRoutes.movePieceEndpoint.serverLogic(request => Unsafe.unsafe(implicit unsafe => runtime.unsafe.runToFuture(chessRoutes.movePieceAndPropagateMessage(request)))))
      .thenRunLogic()
      .backend()

    val request: MovePieceRequest = MovePieceRequest(1, 0, 2)

    val response = basicRequest
      .post(uri"http://test.com/app/movePiece")
      .body(request)(movePieceRequestSerializer)
      .send(backendStub)

    response.map(_.code.code shouldBe 200)
  }

  "removePieceEndpoint" should "return success response for a valid piece removal" in {
    implicit val removePieceRequestSerializer: BodySerializer[RemovePieceRequest] = { request: RemovePieceRequest =>
      val serialized = request.asJson.noSpaces
      StringBody(serialized, "UTF-8", MediaType.ApplicationJson)
    }

    val backendStub = TapirStubInterpreter(SttpBackendStub.asynchronousFuture)
      .whenServerEndpoint(chessRoutes.removePieceEndpoint.serverLogic(request => Unsafe.unsafe(implicit unsafe => runtime.unsafe.runToFuture(chessRoutes.removePieceAndPropagateMessage(request)))))
      .thenRunLogic()
      .backend()

    val request: RemovePieceRequest = RemovePieceRequest(2)

    val response = basicRequest
      .post(uri"http://test.com/app/removePiece")
      .body(request)(removePieceRequestSerializer)
      .send(backendStub)

    response.map(_.code.code shouldBe 200)
  }

}