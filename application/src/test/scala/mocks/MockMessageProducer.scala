package mocks

import kafka.MessageProducer
import zio.{Task, ULayer, ZIO, ZLayer}

object MockMessageProducer extends MessageProducer {
  override def handleAction(actionType: String, message: String): Task[Unit] = ZIO.unit

  val layer: ULayer[MessageProducer] = ZLayer.succeed(MockMessageProducer)
}
