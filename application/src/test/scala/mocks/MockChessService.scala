package mocks

import domain._
import services.ChessService
import zio.{Task, ULayer, ZIO, ZLayer}

object MockChessService extends ChessService {
  override def addPiece(piece: ChessPiece): Task[Either[String, ActionResponse]] =
    if (piece.pieceType == PieceType.Rook || piece.pieceType == PieceType.Bishop)
      ZIO.succeed(Right(AddPieceResponse(piece.id)))
    else
      ZIO.succeed(Left("No such piece type"))

  override def removePiece(pieceId: PieceId): Task[Either[String, ActionResponse]] =
    ZIO.succeed(Right(RemovePieceResponse(pieceId)))

  override def movePiece(pieceId: PieceId, position: Position): Task[Either[String, ActionResponse]] =
    ZIO.succeed(Right(MovePieceResponse(pieceId, position)))

  override def clearBoard: Task[Either[String, ActionResponse]] =
    ZIO.succeed(Right(ClearBoardResponse(true)))

  val layer: ULayer[ChessService] = ZLayer.succeed(MockChessService)
}
