package domain

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ChessBoardSpec extends AnyFlatSpec with Matchers {

  "A ChessBoard" should "allow adding a piece to an empty position" in {
    val board = ChessBoard.empty
    val rook = Rook(PieceId(1), Position(0, 0))
    val result = board.addPiece(rook)
    result shouldBe Right(board.copy(pieces = Map(rook.id -> rook)))
  }

  it should "not allow adding a piece to an occupied position" in {
    val rook1 = Rook(PieceId(1), Position(0, 0))
    val board = ChessBoard(Map(rook1.id -> rook1))
    val rook2 = Rook(PieceId(2), Position(0, 0))
    val result = board.addPiece(rook2)
    result shouldBe Left("Position already occupied")
  }

  it should "not allow adding a piece to an invalid position" in {
    val board = ChessBoard.empty
    val rook = Rook(PieceId(1), Position(8, 8))
    val result = board.addPiece(rook)
    result shouldBe Left("Position out of bounds")
  }

  it should "allow removing an existing piece" in {
    val rook = Rook(PieceId(1), Position(0, 0))
    val board = ChessBoard(Map(rook.id -> rook))
    val result = board.removePiece(rook.id)
    result shouldBe Right(board.copy(pieces = Map.empty))
  }

  it should "not allow removing a non-existent piece" in {
    val board = ChessBoard.empty
    val result = board.removePiece(PieceId(1))
    result shouldBe Left("Piece not found")
  }

  it should "allow moving a piece to a valid position" in {
    val rook = Rook(PieceId(1), Position(0, 0))
    val board = ChessBoard(Map(rook.id -> rook))
    val newPosition = Position(0, 5)
    val result = board.movePiece(rook.id, newPosition)
    val updatedRook = rook.copy(position = newPosition)
    result shouldBe Right(board.copy(pieces = Map(rook.id -> updatedRook)))
  }

  it should "not allow moving a piece to an invalid position" in {
    val rook = Rook(PieceId(1), Position(0, 0))
    val board = ChessBoard(Map(rook.id -> rook))
    val newPosition = Position(8, 8)
    val result = board.movePiece(rook.id, newPosition)
    result shouldBe Left("Position out of bounds")
  }

  it should "not allow moving a piece in an invalid way" in {
    val rook = Rook(PieceId(1), Position(0, 0))
    val board = ChessBoard(Map(rook.id -> rook))
    val newPosition = Position(1, 1)
    val result = board.movePiece(rook.id, newPosition)
    result shouldBe Left("Invalid move")
  }

  it should "not allow moving a non-existent piece" in {
    val board = ChessBoard.empty
    val newPosition = Position(0, 5)
    val result = board.movePiece(PieceId(1), newPosition)
    result shouldBe Left("Piece not found")
  }
}
