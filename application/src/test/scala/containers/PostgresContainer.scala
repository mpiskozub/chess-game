package containers

import cats.effect.Resource
import doobie.hikari.HikariTransactor
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.containers.PostgreSQLContainer.IMAGE
import zio.Task
import zio.interop.catz.asyncInstance

import java.util.concurrent.Executors
import scala.concurrent.ExecutionContext

sealed trait PostgresContainer {

  private val initScriptPath: String = "test-init.sql"

  def createPostgresContainer: PostgreSQLContainer[?] = new PostgreSQLContainer(IMAGE)
    .withInitScript(initScriptPath)

  val connectEC = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(8))

  def createHikariTransactor(container: PostgreSQLContainer[?]): Resource[Task, HikariTransactor[Task]] = for {
    t <- HikariTransactor.newHikariTransactor[Task](
      container.getDriverClassName,
      container.getJdbcUrl,
      container.getUsername,
      container.getPassword,
      connectEC = connectEC
    )
  } yield t

}

object PostgresContainer extends PostgresContainer
