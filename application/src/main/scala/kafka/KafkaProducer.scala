package kafka

import configuration.AppConfiguration
import org.apache.kafka.clients.producer.ProducerRecord
import zio._
import zio.kafka.producer.{Producer, ProducerSettings}
import zio.kafka.serde.Serde

trait MessageProducer {
  def handleAction(actionType: String, description: String): RIO[Producer, Unit]
}

final case class KafkaProducer() extends AppConfiguration with MessageProducer {

  private val producerSettings: ProducerSettings =
    ProducerSettings(List(configuration.kafka.address))
      .withClientId(configuration.kafka.client)

  val makeProducerLayer: ZLayer[Any, Throwable, Producer] = ZLayer.scoped(Producer.make(producerSettings))

  private def produceMessage(topic: String, key: String, value: String): RIO[Producer, Unit] = {
    val record = new ProducerRecord[String, String](topic, key, value)
    Producer
      .produce(record, Serde.string, Serde.string)
      .tap { _ =>
        ZIO.log(s"Message produced: $key -> $value")
      }
      .unit
  }

  // Function to handle actions and emit them to Kafka
  override def handleAction(actionType: String, description: String): RIO[Producer, Unit] = {
    val topic = "events"
    val key   = s"$actionType"
    val value = description
    produceMessage(topic, key, value)
  }

}
