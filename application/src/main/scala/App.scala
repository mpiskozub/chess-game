import cats.effect.Resource
import db.PostgresChessRepositoryLive
import doobie.hikari.HikariTransactor
import http.ChessRoutes
import kafka.KafkaProducer
import services.ChessService
import zio.http.Server
import zio.interop.catz.asyncInstance
import zio.{Scope, Task, ZIO, ZIOAppArgs, ZIOAppDefault}

import java.util.concurrent.Executors
import scala.concurrent.ExecutionContext

object App extends ZIOAppDefault {
  override def run: ZIO[Any with ZIOAppArgs with Scope, Any, Any] = {

    val connectEC = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(8))

    val transactor: Resource[Task, HikariTransactor[Task]] = for {
      t <- HikariTransactor.newHikariTransactor[Task](
        driverClassName = "org.postgresql.Driver",
        url = "jdbc:postgresql://localhost:5432/chess",
        user = "chess_user",
        pass = "pass",
        connectEC = connectEC
      )
    } yield t

    val postgresChessRepository          = PostgresChessRepositoryLive(transactor)
    val chessService: Task[ChessService] = ChessService.make(postgresChessRepository)
    val routes                           = ChessRoutes(chessService, KafkaProducer()).chessRoutes

    Server.serve(routes).provide(Server.defaultWithPort(8080))
  }
}
