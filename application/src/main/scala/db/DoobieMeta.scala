package db

import domain.PieceType
import domain.PieceType.PieceType
import doobie.Meta

object DoobieMeta {
  implicit val pieceTypeMeta: Meta[PieceType] = Meta[String].imap(PieceType.withName)(_.toString)
}
