package db

import cats.effect.Resource
import domain.{Bishop, ChessBoard, ChessPiece, PieceId, Position, Rook}
import doobie.hikari.HikariTransactor
import doobie.implicits._
import zio._
import zio.interop.catz._
import DoobieMeta._

trait PostgresChessRepository {
  def addPiece(piece: ChessPiece): Task[Either[String, Int]]
  def removePiece(id: PieceId): Task[Either[String, Int]]
  def movePiece(id: PieceId, position: Position): Task[Either[String, Int]]
  def loadBoard: Task[Either[String, ChessBoard]]
  def clearBoard: Task[Either[String, Int]]
}

final case class PostgresChessRepositoryLive(transactor: Resource[Task, HikariTransactor[Task]])
    extends PostgresChessRepository {
  def addPiece(piece: ChessPiece): Task[Either[String, Int]] = {
    val query = sql"""
      INSERT INTO pieces (id, x_position, y_position, piece_type, removed)
      VALUES (${piece.id}, ${piece.position.x}, ${piece.position.y}, ${piece.pieceType}, ${piece.removed})
    """.update.run

    transactor.use { xa =>
      query.transact(xa).either.map {
        case Right(value) => Right(value)
        case Left(e)      => Left(e.getMessage)
      }
    }
  }

  def removePiece(id: PieceId): Task[Either[String, Int]] = {
    val query =
      sql"""
        UPDATE pieces
        SET removed = true
        WHERE id = ${id.value}
      """.update.run

    transactor.use { xa =>
      query.transact(xa).either.map {
        case Right(value) => Right(value)
        case Left(e)      => Left(e.getMessage)
      }
    }
  }

  def movePiece(id: PieceId, position: Position): Task[Either[String, Int]] = {
    val query =
      sql"""
        UPDATE pieces
        SET x_position = ${position.x}, y_position = ${position.y}
        WHERE id = ${id.value} AND removed = false
      """.update.run

    transactor.use { xa =>
      query.transact(xa).either.map {
        case Right(value) => Right(value)
        case Left(e)      => Left(e.getMessage)
      }
    }
  }

  def loadBoard: Task[Either[String, ChessBoard]] = {
    val query = sql"""
    SELECT id, x_position, y_position, piece_type
    FROM pieces
    WHERE removed = false
  """.query[(Int, Int, Int, String)].to[List]

    transactor.use { xa =>
      query.transact(xa).either.map {
        case Right(results) =>
          val pieces = results.collect {
            case (id, x, y, "Rook")   => PieceId(id) -> Rook(PieceId(id), Position(x, y))
            case (id, x, y, "Bishop") => PieceId(id) -> Bishop(PieceId(id), Position(x, y))
          }.toMap
          Right(ChessBoard(pieces))
        case Left(e) => Left(e.getMessage)
      }
    }
  }

  def clearBoard: Task[Either[String, Int]] = {
    val query =
      sql"""
        DELETE FROM pieces
      """.update.run

    transactor.use { xa =>
      query.transact(xa).either.map {
        case Right(value) => Right(value)
        case Left(e)      => Left(e.getMessage)
      }
    }
  }
}
