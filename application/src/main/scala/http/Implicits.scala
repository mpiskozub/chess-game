package http

import domain.PieceType.PieceType
import domain.{AddPieceRequest, MovePieceRequest, PieceType, RemovePieceRequest}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import io.circe.{Decoder, Encoder}

object Implicits {
  implicit val pieceTypeEncoder: Encoder[PieceType]                   = Encoder.encodeEnumeration(PieceType)
  implicit val pieceTypeDecoder: Decoder[PieceType]                   = Decoder.decodeEnumeration(PieceType)
  implicit val addPieceRequestEncoder: Encoder[AddPieceRequest]       = deriveEncoder[AddPieceRequest]
  implicit val addPieceRequestDecoder: Decoder[AddPieceRequest]       = deriveDecoder[AddPieceRequest]
  implicit val movePieceRequestEncoder: Encoder[MovePieceRequest]     = deriveEncoder[MovePieceRequest]
  implicit val movePieceRequestDecoder: Decoder[MovePieceRequest]     = deriveDecoder[MovePieceRequest]
  implicit val removePieceRequestEncoder: Encoder[RemovePieceRequest] = deriveEncoder[RemovePieceRequest]
  implicit val removePieceRequestDecoder: Decoder[RemovePieceRequest] = deriveDecoder[RemovePieceRequest]
}
