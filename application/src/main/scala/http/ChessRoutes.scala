package http

import domain._
import io.circe.generic.auto._
import services.ChessService
import sttp.tapir._
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.server.ziohttp.ZioHttpInterpreter
import zio.http.HttpApp
import zio.{Task, ZIO}
import Implicits._
import domain.ActionType.ActionType
import domain.ErrorModel.UnsupportedPieceTypeException
import kafka.MessageProducer
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.swagger.bundle.SwaggerInterpreter

final case class ChessRoutes(chessService: Task[ChessService], producer: MessageProducer) {

  val addPieceEndpoint: Endpoint[Unit, AddPieceRequest, String, ActionResponse, Any] = endpoint.post
    .in("app" / "addPiece")
    .in(jsonBody[AddPieceRequest])
    .out(jsonBody[ActionResponse])
    .errorOut(stringBody)

  val removePieceEndpoint: Endpoint[Unit, RemovePieceRequest, String, ActionResponse, Any] = endpoint.post
    .in("app" / "removePiece")
    .in(jsonBody[RemovePieceRequest])
    .out(jsonBody[ActionResponse])
    .errorOut(stringBody)

  val movePieceEndpoint: Endpoint[Unit, MovePieceRequest, String, ActionResponse, Any] = endpoint.post
    .in("app" / "movePiece")
    .in(jsonBody[MovePieceRequest])
    .out(jsonBody[ActionResponse])
    .errorOut(stringBody)

  val clearBoardEndpoint: Endpoint[Unit, Unit, String, ActionResponse, Any] = endpoint.post
    .in("app" / "clearBoard")
    .out(jsonBody[ActionResponse])
    .errorOut(stringBody)

  private def swaggerEndpoints: List[ServerEndpoint[Any, Task]] = {
    SwaggerInterpreter().fromEndpoints[Task](
      List(addPieceEndpoint, removePieceEndpoint, movePieceEndpoint, clearBoardEndpoint),
      "Chess API",
      "1.0"
    )
  }

  def addPieceAndPropagateMessage(request: AddPieceRequest): Task[Either[String, ActionResponse]] = {
    val addPieceAction = request.pieceType match {
      case PieceType.Rook =>
        chessService.flatMap(service => service.addPiece(Rook(PieceId(request.id), Position(request.x, request.y))))
      case PieceType.Bishop =>
        chessService.flatMap(service => service.addPiece(Bishop(PieceId(request.id), Position(request.x, request.y))))
      case _ => ZIO.fail(UnsupportedPieceTypeException("No such piece type"))
    }

    propagateSuccessMessage(addPieceAction, ActionType.AddPiece)
    addPieceAction
  }

  def removePieceAndPropagateMessage(request: RemovePieceRequest): Task[Either[String, ActionResponse]] = {
    val removePieceAction = chessService.flatMap(service => service.removePiece(PieceId(request.id)))
    propagateSuccessMessage(removePieceAction, ActionType.RemovePiece)
    removePieceAction
  }

  def movePieceAndPropagateMessage(request: MovePieceRequest): Task[Either[String, ActionResponse]] = {
    val movePieceAction =
      chessService.flatMap(service => service.movePiece(PieceId(request.id), Position(request.x, request.y)))
    propagateSuccessMessage(movePieceAction, ActionType.MovePiece)
    movePieceAction
  }

  def clearBoard(): Task[Either[String, ActionResponse]] = {
    chessService.flatMap(service => service.clearBoard)
  }

  val chessRoutes: HttpApp[Any] =
    ZioHttpInterpreter().toHttp(
      List(
        addPieceEndpoint.serverLogic(request => addPieceAndPropagateMessage(request)),
        removePieceEndpoint.serverLogic(request => removePieceAndPropagateMessage(request)),
        movePieceEndpoint.serverLogic(request => movePieceAndPropagateMessage(request)),
        clearBoardEndpoint.serverLogic(_ => clearBoard())
      ) ++ swaggerEndpoints
    )

  private def propagateSuccessMessage(
    pieceAction: ZIO[Any, Throwable, Either[String, ActionResponse]],
    actionType: ActionType
  ): ZIO[Any, Throwable, Unit] = {
    pieceAction.flatMap {
      case Right(response) =>
        producer.handleAction(actionType.toString, response.message)
        ZIO.log(response.message)
      case Left(errorMessage) => ZIO.log(errorMessage)
    }
  }
}
