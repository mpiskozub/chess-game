package domain

import domain.PieceType.PieceType

sealed trait ChessPiece {
  def id: PieceId
  def position: Position
  def pieceType: PieceType
  def removed: Boolean = false
}

case class Rook(id: PieceId, position: Position) extends ChessPiece {
  override def pieceType: PieceType = PieceType.Rook
}
case class Bishop(id: PieceId, position: Position) extends ChessPiece {
  override def pieceType: PieceType = PieceType.Bishop
}
case class Position(x: Int, y: Int)
case class PieceId(value: Int)

object PieceType extends Enumeration {
  type PieceType = Value
  val Rook, Bishop = Value
}

object ActionType extends Enumeration {
  type ActionType = Value
  val AddPiece, RemovePiece, MovePiece = Value
}

case class AddPieceRequest(id: Int, x: Int, y: Int, pieceType: PieceType)
case class RemovePieceRequest(id: Int)
case class MovePieceRequest(id: Int, x: Int, y: Int)

sealed trait ActionResponse {
  def message: String
}

case class AddPieceResponse(id: PieceId) extends ActionResponse {
  override def message: String = s"Successfully added piece with id ${id.value}"
}

case class RemovePieceResponse(id: PieceId) extends ActionResponse {
  override def message: String = s"Successfully removed piece with id ${id.value}"
}

case class MovePieceResponse(id: PieceId, position: Position) extends ActionResponse {
  override def message: String =
    s"Successfully moved piece with id ${id.value} to position (${position.x}, ${position.y})"
}

case class ClearBoardResponse(isCleared: Boolean) extends ActionResponse {
  override def message: String = s"Successfully removed all pieces from the board"
}
