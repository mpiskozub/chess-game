package domain

object ErrorModel {
  case class UnsupportedPieceTypeException(message: String) extends Throwable(message)
}
