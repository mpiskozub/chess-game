package domain

trait ChessBoardActions {
  def addPiece(piece: ChessPiece): Either[String, ChessBoard]
  def removePiece(id: PieceId): Either[String, ChessBoard]
  def movePiece(id: PieceId, position: Position): Either[String, ChessBoard]
}

final case class ChessBoard(pieces: Map[PieceId, ChessPiece]) extends ChessBoardActions {
  override def addPiece(piece: ChessPiece): Either[String, ChessBoard] = {
    if (!isValidPosition(piece.position)) {
      Left("Position out of bounds")
    } else if (pieces.values.exists(_.position == piece.position)) {
      Left("Position already occupied")
    } else {
      Right(copy(pieces = pieces + (piece.id -> piece)))
    }
  }

  override def removePiece(id: PieceId): Either[String, ChessBoard] = {
    if (pieces.contains(id)) {
      Right(copy(pieces = pieces - id))
    } else {
      Left("Piece not found")
    }
  }

  override def movePiece(id: PieceId, newPosition: Position): Either[String, ChessBoard] = {
    if (!isValidPosition(newPosition)) {
      Left("Position out of bounds")
    } else {
      pieces.get(id) match {
        case Some(piece) if isValidMove(piece, newPosition) =>
          val updatedPiece = piece match {
            case r: Rook   => r.copy(position = newPosition)
            case b: Bishop => b.copy(position = newPosition)
          }
          Right(copy(pieces = pieces + (id -> updatedPiece)))
        case Some(_) => Left("Invalid move")
        case None    => Left("Piece not found")
      }
    }
  }

  private def isValidMove(piece: ChessPiece, newPosition: Position): Boolean = {
    piece match {
      case _: Rook   => isValidRookMove(piece.position, newPosition)
      case _: Bishop => isValidBishopMove(piece.position, newPosition)
    }
  }

  private def isValidRookMove(from: Position, to: Position): Boolean = {
    val path = if (from.x == to.x) {
      val range = if (from.y < to.y) from.y + 1 until to.y else to.y + 1 until from.y
      range.map(y => Position(from.x, y)).toList
    } else if (from.y == to.y) {
      val range = if (from.x < to.x) from.x + 1 until to.x else to.x + 1 until from.x
      range.map(x => Position(x, from.y)).toList
    } else {
      return false
    }
    path.forall(pos => !pieces.values.exists(_.position == pos))
  }

  private def isValidBishopMove(from: Position, to: Position): Boolean = {
    if (math.abs(from.x - to.x) == math.abs(from.y - to.y)) {
      val xStep = if (from.x < to.x) 1 else -1
      val yStep = if (from.y < to.y) 1 else -1
      val path  = (1 until math.abs(from.x - to.x)).map(i => Position(from.x + i * xStep, from.y + i * yStep)).toList
      path.forall(pos => !pieces.values.exists(_.position == pos))
    } else {
      false
    }
  }

  private def isValidPosition(position: Position): Boolean = {
    position.x >= 0 && position.x <= 7 && position.y >= 0 && position.y <= 7
  }
}

object ChessBoard {
  def empty: ChessBoard = ChessBoard(Map.empty)
}
