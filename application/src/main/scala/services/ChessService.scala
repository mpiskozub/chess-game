package services

import db.PostgresChessRepository
import domain.{
  ActionResponse,
  AddPieceResponse,
  ChessBoard,
  ChessPiece,
  ClearBoardResponse,
  MovePieceResponse,
  PieceId,
  Position,
  RemovePieceResponse
}
import zio._

trait ChessService {
  def addPiece(piece: ChessPiece): Task[Either[String, ActionResponse]]
  def removePiece(id: PieceId): Task[Either[String, ActionResponse]]
  def movePiece(id: PieceId, newPosition: Position): Task[Either[String, ActionResponse]]
  def clearBoard: Task[Either[String, ActionResponse]]
}

object ChessService {
  def make(repository: PostgresChessRepository): Task[ChessService] = {
    for {
      // Create the Ref for ChessBoard internally
      boardRef <- Ref.make(ChessBoard.empty)
      // Load the initial board state from the repository
      initialBoard <- repository.loadBoard
      _ <- initialBoard match {
        case Right(board) => boardRef.set(board)
        case Left(error)  => ZIO.fail(new Exception(error))
      }
    } yield new ChessService {
      override def addPiece(piece: ChessPiece): Task[Either[String, ActionResponse]] = {
        for {
          // Get the current board state
          currentBoard <- boardRef.get
          // Attempt to add the piece to the board
          result <- currentBoard.addPiece(piece) match {
            case Right(updatedBoard) =>
              // If successful, update the database
              repository.addPiece(piece).flatMap {
                case Right(_) =>
                  // Update the in-memory state
                  boardRef.set(updatedBoard).as(Right(AddPieceResponse(piece.id)))
                case Left(error) => ZIO.succeed(Left(error))
              }
            case Left(error) =>
              // If the piece addition fails, return the error
              ZIO.succeed(Left(error))
          }
        } yield result
      }

      override def removePiece(id: PieceId): Task[Either[String, ActionResponse]] = {
        for {
          currentBoard <- boardRef.get
          result <- currentBoard.removePiece(id) match {
            case Right(updatedBoard) =>
              repository.removePiece(id).flatMap {
                case Right(_) =>
                  boardRef.set(updatedBoard).as(Right(RemovePieceResponse(id)))
                case Left(error) =>
                  ZIO.succeed(Left(error))
              }
            case Left(error) =>
              ZIO.succeed(Left(error))
          }
        } yield result
      }

      override def movePiece(id: PieceId, newPosition: Position): Task[Either[String, ActionResponse]] = {
        for {
          currentBoard <- boardRef.get
          result <- currentBoard.movePiece(id, newPosition) match {
            case Right(updatedBoard) =>
              repository.movePiece(id, newPosition).flatMap {
                case Right(_)    => boardRef.set(updatedBoard).as(Right(MovePieceResponse(id, newPosition)))
                case Left(error) => ZIO.succeed(Left(error))
              }
            case Left(error) =>
              ZIO.succeed(Left(error))
          }
        } yield result
      }

      override def clearBoard: Task[Either[String, ActionResponse]] = {
        for {
          _ <- boardRef.set(ChessBoard.empty).asRight
          result <- repository.clearBoard.map {
            case Right(_)    => Right(ClearBoardResponse(true))
            case Left(error) => Left(error)
          }
        } yield result
      }

    }
  }
}
