import sbt._
import sbt.librarymanagement.ModuleID

object Dependencies {

  private val config = Seq(
    "com.typesafe"           % "config"     % "1.4.3",
    "com.github.pureconfig" %% "pureconfig" % "0.17.6"
  )

  lazy val zio: Seq[ModuleID] = Seq(
    "dev.zio" %% "zio"       % "2.0.21",
    "dev.zio" %% "zio-kafka" % "2.7.4",
    "dev.zio" %% "zio-interop-cats" % "23.1.0.2",
    "dev.zio" %% "zio-test" % "2.0.0" % Test,
    "dev.zio" %% "zio-test-sbt" % "2.0.0" % Test
  )

  lazy val tapir: Seq[ModuleID] = Seq(
    "com.softwaremill.sttp.tapir" %% "tapir-zio-http-server" % "1.10.0",
    "com.softwaremill.sttp.tapir" %% "tapir-json-circe" % "1.10.0",
    "com.softwaremill.sttp.tapir" %% "tapir-openapi-docs" % "1.10.0",
    "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui" % "1.10.0",
    "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-bundle" % "1.10.7",
    "com.softwaremill.sttp.tapir" %% "tapir-sttp-stub-server" % "1.10.7"
    )

  lazy val scalatest: Seq[ModuleID] = Seq(
    "org.scalatest" %% "scalatest" % "3.2.11" % Test,
    "com.dimafeng" %% "testcontainers-scala-scalatest" % "0.40.12" % "test",
    "com.dimafeng" %% "testcontainers-scala-postgresql" % "0.40.12" % "test",
  )

  lazy val circe: Seq[ModuleID] = Seq(
    "io.circe" %% "circe-core" % "0.14.1",
    "io.circe" %% "circe-generic" % "0.14.2",
    "io.circe" %% "circe-parser" % "0.14.2"
  )

  lazy val doobie: Seq[ModuleID] = Seq(
    "org.tpolecat" %% "doobie-core" % "1.0.0-RC4",
    "org.tpolecat" %% "doobie-hikari" % "1.0.0-RC4", // HikariCP transactor.
    "org.tpolecat" %% "doobie-postgres" % "1.0.0-RC4", // Postgres driver 42.3.1 + type mappings.
    "org.tpolecat" %% "doobie-scalatest" % "1.0.0-RC4" % "test",
  )

  val application: Seq[ModuleID] = zio ++ tapir ++ circe ++ doobie ++ config ++ scalatest
  val client: Seq[ModuleID]      = config ++ zio
}
